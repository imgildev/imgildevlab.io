export interface Link {
  title: Record<string, string>;
  url: string;
  icon?: string;
}

export const siteConfig = {
  title: {
    en: 'Manuel Gil',
    es: 'Manuel Gil',
  },
  description: {
    en: 'Software Engineer & Open Source Enthusiast',
    es: 'Ingeniero de Software y Entusiasta del Código Abierto',
  },
  footer: {
    en: 'Made with ❤️ by Manuel Gil',
    es: 'Hecho con ❤️ por Manuel Gil',
  },
};

export const links: Link[] = [
  {
    title: {
      en: 'Projects',
      es: 'Proyectos',
    },
    url: 'https://manuelgil.me/',
    icon: 'projects',
  },
  {
    title: {
      en: 'LinkedIn',
      es: 'LinkedIn',
    },
    url: 'https://www.linkedin.com/in/imgildev',
    icon: 'linkedin',
  },
  {
    title: {
      en: 'GitHub',
      es: 'GitHub',
    },
    url: 'https://github.com/ManuelGil',
    icon: 'github',
  },
  {
    title: {
      en: 'Twitch',
      es: 'Twitch',
    },
    url: 'https://twitch.tv/imgildev',
    icon: 'twitch',
  },
  {
    title: {
      en: 'YouTube',
      es: 'YouTube',
    },
    url: 'https://youtube.com/@imgildev',
    icon: 'youtube',
  },
  {
    title: {
      en: 'Twitter',
      es: 'Twitter',
    },
    url: 'https://twitter.com/imgildev',
    icon: 'twitter',
  },
  {
    title: {
      en: 'Discord',
      es: 'Discord',
    },
    url: 'https://discord.gg/qVbrGpESFU',
    icon: 'discord',
  },
];
