// Type imports
import type { ManifestOptions } from 'vite-plugin-pwa';

/**
 * Defines the default SEO configuration for the website.
 */
export const seoConfig = {
  baseURL: 'https://manuelgil.dev', // Change this to your production URL.
  description:
    'Manuel Gil is a dev & content creator with extensive experience in TypeScript, Angular, Nestjs, and PHP. I am passionate about Open Source.', // Change this to be your website's description.
  type: 'website',
  image: {
    url: 'https://www.gravatar.com/avatar/55ae77beb9988bf27d7a4ecf007e3cf6?s=500', // Change this to your website's thumbnail.
    alt: 'avatar', // Change this to your website's thumbnail description.
    width: 500,
    height: 500,
  },
  siteName: 'Manuel Gil 🦊 | @imgildev | Dev & Content Creator', // Change this to your website's name,
};

/**
 * Defines the configuration for PWA webmanifest.
 */
export const manifest: Partial<ManifestOptions> = {
  name: 'Manuel Gil 🦊 | @imgildev | Dev & Content Creator', // Change this to your website's name.
  short_name: 'Manuel Gil', // Change this to your website's short name.
  description:
    'Manuel Gil is a dev & content creator with extensive experience in TypeScript, Angular, Nestjs, and PHP. I am passionate about Open Source.', // Change this to your websites description.
  theme_color: '#30E130', // Change this to your primary color.
  background_color: '#ffffff', // Change this to your background color.
  display: 'minimal-ui',
};
